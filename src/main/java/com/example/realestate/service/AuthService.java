package com.example.realestate.service;

import com.example.realestate.dto.AuthDto;
import com.example.realestate.entity.User;
import com.example.realestate.enumeration.UserStatus;
import com.example.realestate.exception.ExceptionApi;
import com.example.realestate.exception.PayLoadExceptionItem;
import com.example.realestate.repo.UserRepo;
import com.example.realestate.req.AuthLoginReq;
import com.example.realestate.req.AuthRegisterReq;
import com.example.realestate.res.ApiResponse;
import com.example.realestate.security.JwtTokenUtil;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

@Service
public class AuthService {

    private final BCryptPasswordEncoder passwordEncoder;

    private final JwtTokenUtil jwtTokenUtil;

    private final UserRepo repo;

    public AuthService(BCryptPasswordEncoder passwordEncoder, JwtTokenUtil jwtTokenUtil, UserRepo repo) {
        this.passwordEncoder = passwordEncoder;
        this.jwtTokenUtil = jwtTokenUtil;
        this.repo = repo;
    }

    public AuthDto login(@Validated AuthLoginReq req) {


        User auth = repo.findByEmail(req.getEmail().trim().toLowerCase()).orElseThrow(() -> {
            throw new ExceptionApi(PayLoadExceptionItem.INCORRECT_CREDENTIALS);
        });

        if (auth.getStatus().equals(UserStatus.DISABLED))
        {
            throw new ExceptionApi(PayLoadExceptionItem.ACCOUNT_DISABLED);
        }

        try {
            if (passwordEncoder.matches(req.getPassword(), auth.getPassword())) {
                AuthDto authDto=new AuthDto();
                authDto.setToken(jwtTokenUtil.generateToken(auth.getId()));
                return authDto;
            } else {
                throw new ExceptionApi(PayLoadExceptionItem.INCORRECT_CREDENTIALS);
            }
        } catch (Exception e) {
            throw new ExceptionApi(PayLoadExceptionItem.INCORRECT_CREDENTIALS);
        }

    }

    public ApiResponse register(AuthRegisterReq registerReq) {

        User member=new User();

        if (!isEmailValid(registerReq.getEmail())) throw new IllegalArgumentException("Invalid Email Format");
        //if (!isPhoneNumberValid(registerReq.getPhone())) throw new IllegalArgumentException("Invalid phone number  Format");

        if(repo.existsByEmailAndDeletedIsFalse(registerReq.getEmail())) throw  new IllegalArgumentException("Email already exist");

        member.setEmail(registerReq.getEmail());
        member.setBirthday(registerReq.getBirthday());
        member.setFirstName(registerReq.getFirstName());
        member.setLastName(registerReq.getLastName());
        member.setPassword(passwordEncoder.encode(registerReq.getPassword()));
        member.setStatus(UserStatus.ENABLED);

        repo.save(member);

        return ApiResponse.builder().id(member.getId()).build();

    }

    public boolean isEmailValid(String email){
        return EmailValidator.getInstance().isValid(email);
    }

}
