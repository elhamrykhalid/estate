package com.example.realestate.dto;

import lombok.Data;


import java.time.LocalDateTime;

@Data
public class Dto {
    private String id;
    private LocalDateTime createdAt;
}
