package com.example.realestate.res;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ApiResponse {
    private String id;
}
