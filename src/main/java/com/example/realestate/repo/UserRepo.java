package com.example.realestate.repo;

import com.example.realestate.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepo extends JpaRepository<User,String > {

    Optional<User> findByEmail(String toLowerCase);

    boolean existsByEmailAndDeletedIsFalse(String email);


}
