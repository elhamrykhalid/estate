
package com.example.realestate.entity;


import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;


@Data
@MappedSuperclass
public abstract class Common implements Serializable {

    @Id
    @Column(unique = true, nullable = false)
    private String id;
    @Column
    private LocalDateTime createdAt;
    @Column
    private LocalDateTime updateAt;
    @Column
    private LocalDate localDate;
    @Column
    private boolean deleted;

    public void id(){
        if (id==null) {
            id = UUID.randomUUID().toString().replace("-", "");
        }
    }

    @PrePersist
    public void prePersist(){
     id();
     setDeleted(false);
     setCreatedAt(LocalDateTime.now());
     setLocalDate(LocalDate.now());
    }

    @PreUpdate
    public void preUpdate(){
       setUpdateAt(LocalDateTime.now());
    }

}
