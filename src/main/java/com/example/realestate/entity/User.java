package com.example.realestate.entity;


import com.example.realestate.enumeration.UserStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "customer")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class User extends Common {
    @Column
    private String firstName;

    @Column
    private String lastName;

    @Column
    private String email;


    @Column
    @JsonIgnore
    private String password;

    @Column
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthday;



    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private UserStatus status;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User obj = (User) o;
        return Objects.equals(getId(), obj.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }



}
