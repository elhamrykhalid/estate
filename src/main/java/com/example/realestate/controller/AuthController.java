package com.example.realestate.controller;

import com.example.realestate.dto.AuthDto;
import com.example.realestate.req.AuthLoginReq;
import com.example.realestate.req.AuthRegisterReq;
import com.example.realestate.res.ApiResponse;
import com.example.realestate.service.AuthService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/auth")
@CrossOrigin
public class AuthController {

    private final AuthService authService;

    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping("/register")
    public ResponseEntity<ApiResponse> register(@RequestBody AuthRegisterReq registerReq){
        return new ResponseEntity<>( authService.register(registerReq), HttpStatus.OK);

    }


    @PostMapping("/login")
    public ResponseEntity<AuthDto>  login(@Valid @RequestBody AuthLoginReq registerReq){
        return new ResponseEntity<>( authService.login(registerReq), HttpStatus.OK);
    }

}
