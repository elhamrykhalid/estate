package com.example.realestate.req;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;

@Data
public class AuthLoginReq {

    private String email;
    private String password;

}
