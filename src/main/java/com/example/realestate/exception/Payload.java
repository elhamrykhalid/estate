package com.example.realestate.exception;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.Date;
import java.util.List;

import static com.fasterxml.jackson.annotation.JsonFormat.Shape.STRING;


@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Payload {
    private int responseCode;
    private String messageError;
    private String detail;
    private Integer statusCode;
    private List<Error> errors;

    @JsonFormat(shape = STRING, pattern = "dd-MM-yyyy'T'HH:mm:ss")
    @Builder.Default
    private Date responseDate = new Date();
}