package com.example.realestate.exception;



public enum PayLoadExceptionItem {

    SYSTEM_ERROR("API SYSTEM ERROR", 99),
    ERROR("", 100),
    INVALID_INPUT("Invalid INPUT", 101),
    HTTP_CALL_ERROR("Http call error", 102),
    NOT_FOUND_USER_SECURITY_CONTEXT("not found user security context", 111),
    COUNTRY_NOT_FOUND("COUNTRY_NOT_FOUND", 113),
    CITY_NOT_FOUND("CITY_NOT_FOUND", 114),
    TASK_NOT_FOUND("TASK_NOT_FOUND", 114),
    USER_NOT_FOUND("USER_NOT_FOUND", 115),
    INCORRECT_CREDENTIALS("INCORRECT_CREDENTIALS", 115),
    ACCOUNT_DISABLED("ACCOUNT_DISABLED", 144),
    COMPANY_NOT_FOUND("COMPANY_NOT_FOUND", 145),  DEMAND_INVOICE_FIRST("DEMAND_INVOICE_FIRST", 145),
    ORDER_UNPAID("ORDER_UNPAID", 145),
    ACCOUNT_DELETED("ACCOUNT_DELETED", 146),
    RC_ALREADY_EXISTS("RC_ALREADY_EXISTS", 147),
    COMMENT_NOT_FOUND("COMMENT_NOT_FOUND", 148),
    ADRESS_EMPTY("ADRESS_EMPTY", 148),
    NO_TICKET_FOUND("NO_TICKET_FOUND", 149),
    MESSAGE_CANT_BE_NULL("MESSAGE_CANT_BE_NULL", 150),
    MUST_BE_GREATER_THAN_INITIAL_PRICE("MUST_BE_GREATER_THAN_INITIAL_PRICE", 151),
    CANNOT_ACCEPT_FINISHED_BET("CANNOT_ACCEPT_FINISHED_BET", 152),
    ROLE_NOT_FOUND("ROLE_NOT_FOUND", 153),
    PRIVILEGE_NOT_FOUND("PRIVILEGE_NOT_FOUND", 154),
    COMPANY_ALREADY_CREATED("COMPANY_ALREADY_CREATED", 155),
    ACCOUNT_BLOCKED("ACCOUNT_BLOCKED", 156),

    RECEIVER_CANT_BE_THE_SENDER("RECEIVER_CANT_BE_THE_SENDER",159),
    MESSAGE_CONTENT_CANT_BE_NULL("MESSAGE_CONTENT_CANT_BE_NULL",152),
    MESSAGE_NOT_FOUND ("MESSAGE_NOT_FOUND",160),
    LOCATION_NOT_NULL("LOCATION_NOT_NULL", 115),
    ZIP_NOT_FOUND("ZIP_NOT_NULL", 115),
    SELECT_IMG("SELECT_IMG",300),
    MAX_CHARACHTERS_TITLE("MAX_CHARACHTERS_TITLE",170),
    MAX_CHARACHTERS_PRICE("MAX_CHARACHTERS_PRICE",150), USER_DOES_NOT_HAVE_BIRTHDAY("USER_DOES_NOT_HAVE_BIRTHDAY",150),
    DEMAND_CANNOT_UPDATED("DEMAND_CANNOT_UPDATED",400);

    private String message;
    private String detail;
    private Integer statusCode;

    PayLoadExceptionItem(String message, Integer statusCode) {
        this.message = message;
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public PayLoadExceptionItem detail(String d) {
        this.detail = d;
        return this;
    }

    public PayLoadExceptionItem message(String m) {
        this.message = m;
        return this;
    }

    public String getDetail() {
        return detail;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    @Override
    public String toString() {
        return (
                "PayLoadExceptionItem{" +
                        "message='" +
                        message +
                        '\'' +
                        ", detail='" +
                        detail +
                        '\'' +
                        ", statusCode=" +
                        statusCode +
                        '}'
        );
    }
}

