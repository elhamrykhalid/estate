package com.example.realestate.security;

import lombok.Data;



@Data
public class JwtPayload {
    private String userId;
    private long iat;
    private long exp;
}

