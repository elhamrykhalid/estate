package com.example.realestate.security;

import com.example.realestate.entity.User;
import com.example.realestate.exception.ExceptionApi;
import com.example.realestate.exception.PayLoadExceptionItem;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class SecurityContextService {
    public User getAuthPrincipal() {
        AuthPrincipal ap = (AuthPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (ap == null || ap.getUser() == null) {
            throw new ExceptionApi(PayLoadExceptionItem.NOT_FOUND_USER_SECURITY_CONTEXT);
        }
        return ap.getUser();
    }
}
