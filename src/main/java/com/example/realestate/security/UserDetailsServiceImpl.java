package com.example.realestate.security;

import com.example.realestate.entity.User;
import com.example.realestate.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;



@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepo repo;

    @Override
    public AuthPrincipal loadUserByUsername(String id) throws UsernameNotFoundException {
        User user = repo.findById(id)
                .orElseThrow(() -> new UsernameNotFoundException("User NOT Found"));
        return AuthMapper.userToAuth(user);
    }
}

