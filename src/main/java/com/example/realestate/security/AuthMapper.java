package com.example.realestate.security;

import com.example.realestate.entity.User;
import com.example.realestate.enumeration.UserStatus;
import org.springframework.security.core.GrantedAuthority;

import java.util.ArrayList;
import java.util.List;



public interface AuthMapper {
    static AuthPrincipal userToAuth(User user) {
        AuthPrincipal userp = new AuthPrincipal();
        userp.setEmail(user.getEmail());
        userp.setPassword(user.getPassword());
        userp.setEnabled(user.getStatus().equals(UserStatus.ENABLED));
        List<GrantedAuthority> authorities = new ArrayList<>();


        userp.setAuthorities(authorities);
        userp.setUser(user);
        return userp;
    }
}

