package com.example.realestate.enumeration;

public enum UserStatus {
    ENABLED,
    DISABLED
}
