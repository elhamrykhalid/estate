FROM adoptopenjdk/maven-openjdk11 AS builder
WORKDIR /app
COPY . .
RUN ls -lia
RUN mvn clean package -DskipTests

FROM adoptopenjdk/openjdk11:alpine
WORKDIR /app
COPY --from=builder /app/target/spring-boot.jar /usr/local/lib/app.jar
EXPOSE 8082
ENTRYPOINT ["java", "-jar", "/usr/local/lib/app.jar"]
HEALTHCHECK --interval=30s --timeout=5s CMD curl -f http:/ /localhost:8082/actuator/health || exit 1